<?php
session_start();
if (!isset($_SESSION['loggedIn']) || !isset($_SESSION['id']) || !isset($_SESSION['username'])) {
  header("Location:../components/login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="../reset.css" />

  <link rel="stylesheet" href="./cms-style.css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


  <title>CMS:Content Management System</title>
</head>

<body>
  <div class="container">
    <h2>Welcome <?php echo $_SESSION['username']; ?></h2> Click here to <a href="./cms-config/log-out.php">Logout</a>
    <!-- <?php
          echo '<pre>';
          var_dump($_SESSION);
          echo '</pre>';
          ?> -->
    <div class="row justify-content-center">

      <div class="col-md-6">
        <div class="ads-box">
          <h2> إضافة إعلان</h2>
          <br>
          <br>
          <form action="./cms-config/ads.php" method="post">
            <div class="form-group">
              <label>
                نص اﻹعلان </label>
              <input type="text" name="text" class="form-control" placeholder="نص اﻹعلان" required>
              <br>
            </div>
            <button type="submit" name="save" class="btn btn-primary"> إضافة إعلان</button>
          </form>
        </div>
      </div>



      <div class="col-md-6">
        <div class="vids-box">
          <h2> إضافة مقطع فيديو</h2>
          <br>
          <br>
          <form action="./cms-config/vids.php" method="post">
            <div class="form-group">
              <label>
                رابط التحميل </label>
              <input type="url" name="url" class="form-control" placeholder="رابط التحميل" required>

              <br>
              <label>
                الوصف</label>
              <input type="text" name="title" class="form-control" placeholder="الوصف" required>
              <br>

            </div>
            <button type="submit" name="save" class="btn btn-primary"> إضافة مقطع فيديو</button>
          </form>
        </div>
      </div>
    </div>
    <!-- <button class="btn btn-primary" id="results" name="results"> عرض النتائج </button> -->
  </div>



  <div class="preview">
    <!-- <div class="container"> -->
    <div class="row justify-content-center">

      <div class="col-md-6">
        <div class="ads-box">
          <h2>اﻹعلانات</h2>
          <?php
          include '../config/dbconnect.php';
          $result = mysqli_query($conn, "SELECT * FROM ads");
          if (mysqli_num_rows($result) > 0) {
          ?>
            <table>
              <tr>
                <td>ID </td>
                <td>Content</td>
                <td>Edit</td>
                <td>Delete</td>
              </tr>
              <?php
              while ($row = mysqli_fetch_array($result)) {
              ?>
                <tr>
                  <td><?php echo $row['id'] ?></td>
                  <td><?php echo $row['content'] ?></td>

                  <td><a href="./cms-config/edit_ads.php?id=<?php echo $row['id']; ?>&t=ads">Edit</a></td>

                  <td><a href="./cms-config/delete.php?id=<?php echo $row['id']; ?>&t=ads">Delete</a></td>
                </tr>
              <?php } ?>
            </table>
          <?php
          } else {
            echo "no results found!";
          }
          ?>
          </table>
        </div>
      </div>

      <div class="col-md-6">
        <div class="vids-box">
          <h2>مقاطع الفيديو</h2>
          <?php
          include '../config/dbconnect.php';
          $result = mysqli_query($conn, "SELECT * FROM vids");
          if (mysqli_num_rows($result) > 0) {
          ?>
            <table>
              <tr>
                <td>ID </td>
                <td>Url</td>
                <td>Title</td>
                <td>Edit</td>
                <td>Delete</td>
              </tr>
              <?php
              while ($row = mysqli_fetch_array($result)) {
              ?>
                <tr>
                  <td><?php echo $row['id'] ?></td>

                  <td><?php echo $row['url'] ?></td>

                  <td><?php echo $row['title'] ?></td>

                  <td><a href="./cms-config/edit_vids.php?id=<?php echo $row['id']; ?>&t=vids">Edit</a></td>
                  <td><a href="./cms-config/delete.php?id=<?php echo $row['id']; ?>&t=vids">Delete</a></td>
                </tr>
              <?php } ?>
            </table>
          <?php
          } else {
            echo "no results found!";
          }
          ?>
          </table>


        </div>
      </div>

    </div>
  </div>




</body>

</html>