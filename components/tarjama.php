<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="../reset.css" />
  <link rel="stylesheet" href="../styles.css" />

  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous" />
  <title> ترجمة الشيخ </title>
</head>

<body>

  <?php include('../templates/header.php'); ?>



  <main>
    <div class="container">
      <img src="../assets/maskGroup1.png" alt="maskGroup" id="hero" />
    </div>

    <div class="container">

      <div class="bio">
        <h2> ترجمة الشيخ</h2>
        <p>.الشيخ محمد فواز النمر هو إمام المسجد اﻷموي في دمشق</p>
      </div>

    </div>


  </main>


  <?php include('../templates/footer.php'); ?>


</body>

</html>