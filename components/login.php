<?php
session_start()
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="../reset.css" />

  <link rel="stylesheet" href="../login-reg-style.css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  <title>تسجيل الدخول</title>
</head>

<body>
  <div class="container">
    <div class="login-box">
      <div class="row justify-content-center">
        <div class="col-md-6">
          <h2> تسجيل الدخول </h2>
          <br>
          <br>
          <form action="../config/validation.php" method="post">
            <div class="form-group">
              <label>
                اسم المستخدم
              </label>
              <input type="text" name="user" class="form-control" placeholder="username" required>
              <br>
              <label>
                كلمة السر
              </label>
              <input type="password" name="pass" class="form-control" required>
            </div>
            <button type="submit" name="save" class="btn btn-primary">تسجيل الدخول</button>
          </form>
        </div>
      </div>
    </div>

  </div>

</body>

</html>