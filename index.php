<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="reset.css" />
  <link rel="stylesheet" href="styles.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css" />



  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous" />
  <title>Project1</title>
</head>

<body>

  <!-- header -->
  <?php include('./templates/header.php'); ?>



  <!-- main -->
  <main>
    <div class="container">
      <img src="./assets/maskGroup1.png" alt="maskGroup" id="hero" />
    </div>


    <!-- ads container -->
    <?php include('./templates/advertisement.php'); ?>


    <div class="container">
      <div class="ruler-dates">
        <p>مواعيد الدروس</p>
        <img src="./assets/titleheader.svg" alt="img" class="ruler-header" />
      </div>
    </div>

    <div class="table">
      <div class="container">
        <table class="table table-responsive-sm table-hover">
          <tbody>
            <tr>
              <td>Thornton</td>
              <td>@fat</td>
              <td>@fat</td>
              <td>Jacob</td>
              <td>Thornton</td>
              <td>@fat</td>
              <td>@fat</td>
              <th scope="row">قبل الفجر</th>
            </tr>
            <tr>
              <td>Thornton</td>
              <td>@fat</td>
              <td>@fat</td>
              <td>Jacob</td>
              <td>Thornton</td>
              <td>@fat</td>
              <td>@fat</td>
              <th scope="row">بعد الفجر</th>
            </tr>
            <tr>
              <!-- <td colspan="2">Larry the Bird</td> -->
              <td>Thornton</td>
              <td>@fat</td>
              <td>@fat</td>
              <td>Jacob</td>
              <td>Thornton</td>
              <td>@fat</td>
              <td>@fat</td>
              <th scope="row">بعد الظهر</th>
            </tr>
          </tbody>
          <thead>
            <tr>
              <th scope="col">الجمعة</th>
              <th scope="col">الخميس</th>
              <th scope="col">اﻷربعاء</th>
              <th scope="col">الثلاثاء</th>
              <th scope="col">اﻹثنين</th>
              <th scope="col">اﻷحد</th>
              <th scope="col">السبت</th>
              <th scope="col">اليوم</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>

    <div class="rect">
      <img src="./assets/Rectangle13.svg" alt="img1" class="img3" />
    </div>




    <!-- lower images section -->
    <?php
    include('./config/dbconnect.php');


    $sql = 'SELECT * FROM vids';

    $result = mysqli_query($conn, $sql);

    $vids = mysqli_fetch_all($result, MYSQLI_ASSOC);

    mysqli_free_result($result);

    mysqli_close($conn);
    ?>

    <div class="wrapper">
      <div class="container" id="lowerImgContainer">
        <div class="row">
          <?php
          foreach ($vids as $vid) {
            $vid_url = $vid['url'];
            $vid_title = $vid['title'];
          ?>
            <div class="col-sm-4">
              <iframe width="225" height="275" src="<?php echo "$vid_url"; ?>" title="YouTube video player" frameborder="0"></iframe>
              <p><?php echo "$vid_title"; ?></p>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>


  </main>


  <!-- footer -->
  <?php include('./templates/footer.php'); ?>


  <!-- jquery -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

  <!-- slick -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      $('.slick-2').slick({
        // autoplay: true,
        autoplaySpeed: 1000,
        slidesToShow: 3,
        // slidesToScroll: 5,
        // speed: 1000,
        dots: true,
        arrows: true,
      });



      $('.slick').slick({
        autoplay: true,
        autoplaySpeed: 100,
        speed: 10000,
        arrows: false,
      });
    });
  </script>


</body>

</html>